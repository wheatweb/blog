<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset the posts table
        DB::table('posts')->truncate();

        $date = Carbon::now();

        // generate 10 dummy posts data
        $posts = [];
        $faker = Factory::create();

        for ($i = 1; $i <= 6; $i++)
        {
            $posts[] = [
                'title' => $faker->sentence(rand(8, 12)),
                'body' => $faker->paragraphs(rand(10, 15), true),
                'created_at' => $date,
                'updated_at' => $date,
            ];
        }

        DB::table('posts')->insert($posts);
    }
}
