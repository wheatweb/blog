<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->truncate();

        $date = Carbon::now();

        DB::table('tasks')->insert([
           [
                'body' => 'Go to the store',
                'completed' => true,
                'created_at' => $date,
                'updated_at' => $date,
           ],
           [
                'body' => 'Finish screencast',
                'completed' => false,
                'created_at' => $date,
                'updated_at' => $date,
           ],
           [
                'body' => 'Clean the house',
                'completed' => false,
                'created_at' => $date,
                'updated_at' => $date,
           ],
        ]);
    }
}
