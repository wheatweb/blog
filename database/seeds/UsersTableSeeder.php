<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset the posts table
        DB::table('users')->truncate();

        $date = Carbon::now();

        DB::table('users')->insert([
            [
                'name' => 'Jane Doe',
                'email' => 'test@testeremail.com',
                'password' => bcrypt('secret'),
                'created_at' => $date,
                'updated_at' => $date
            ],
            [
                'name' => 'John Doe',
                'email' => 'tester@testeremail.com',
                'password' => bcrypt('secret'),
                'created_at' => $date,
                'updated_at' => $date
            ],
            [
                'name' => 'Shaun Wheaton',
                'email' => 'web.shaunwheaton@gmail.com',
                'password' => bcrypt('secret'),
                'created_at' => $date,
                'updated_at' => $date
            ],
        ]);
    }
}
