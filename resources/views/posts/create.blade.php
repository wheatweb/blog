@extends('layouts.master')

@section('content')
<div class="col-md-8 blog-main">
    <h2>Publish a Post</h2>
    <hr>

    <form method="POST" action="/posts">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="postTitle">Title</label>
          <input type="text" name="title" class="form-control" id="postTitle" placeholder="Title">
        </div>
        <div class="form-group">
          <label for="postBody">Body</label>
          <textarea type="textarea" name="body" rows="5 " class="form-control" id="postBody" placeholder="Message"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Publish</button>
    </form>
</div>
@endsection