@extends('layouts.master')

@section('content')
    <div class="col-md-8 blog-main">
      <div class="blog-post">
        <h2 class="blog-post-title">{{ $post->title }}</h2>

        <p class="blog-post-meta">{{ $post->created_at }} by <a href="#">Mark</a></p>
        <hr>

        {!! $post->body_html !!}
      </div><!-- /.blog-post -->
    </div>
@endsection