@extends('layouts.master')

@section('content')
<div class="col-md-8 blog-main">
  <h3 class="pb-4 mb-4 font-italic border-bottom">
    From the Firehose
  </h3>

  @foreach($posts as $post)
  <div class="blog-post">
    <a href="/posts/{{ $post->id }}"><h2 class="blog-post-title">{{ $post->title }}</h2></a>
    <p class="blog-post-meta">{{ $post->created_at }} by <a href="#">Mark</a></p>

    {!! $post->body_html !!}
  </div><!-- /.blog-post -->
  @endforeach

  <nav class="blog-pagination">
    <a class="btn btn-outline-primary" href="#">Older</a>
    <a class="btn btn-outline-secondary disabled" href="#" tabindex="-1" aria-disabled="true">Newer</a>
  </nav>
</div>
@endsection

@section('footer')

@endsection