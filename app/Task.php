<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function scopeCompletedTasks($query)
    {
        return $query->where('completed', 1);
    }

    public function scopeIncompletedTasks($query)
    {
        return $query->where('completed', 0);
    }
}
