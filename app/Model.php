<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

// sw - New model that extends the Eloquent Model to give Model my own properties and behavior
class Model extends Eloquent
{
    protected $guarded = [];
}
