<?php

namespace App;

use GrahamCampbell\Markdown\Facades\Markdown;
// sw - Removed the use of the Eloquent Model to use my own model that extends the Eloquent Model
//use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // sw - Removed the protected fillable to and replaced it by using my Model class to create a protected guarded variable
    // sw - This approach requires more checks to make sure unwanted mass assignments are not used
    //protected $fillable = ['title', 'body'];

    public function getBodyHTMLAttribute($value)
    {
        return $this->body ? Markdown::convertToHTML(e($this->body)) : NULL;
    }

    public function getExcerptHTMLAttribute($value)
    {
        return $this->excerpt ? Markdown::convertToHTML(e($this->excerpt)) : NULL;
    }
}
